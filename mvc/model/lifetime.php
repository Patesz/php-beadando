

<?php
    class Age{
        private $year = null;
        private $month = null;
        private $day = null;
        private $todayDate;

        function __construct()
        {
            $this->todayDate = date('Y m d H i s');         // H i s m d y  -   óra perc másodperc hónap nap év  
        }

        public function setYear($year){
            $this->year = $year;
        }

        public function setMonth($month){
            $this->month = $month;
        }

        public function setDay($day){
            $this->day = $day;
        }      

        public function result(){
            if($this->year != null && $this->month != null && $this->day != null){
                $dateArray = explode(" ", $this->todayDate);
    
                $monthInDays = (31+((28+28+28+29)/4)+31+30+31+30+31+31+30+31+30+31)/12;
    
                $inputInSeconds = mktime(0,0,0,$this->month,$this->day,$this->year);
                $todayInSeconds = mktime($dateArray[3],$dateArray[4],$dateArray[5],$dateArray[1],$dateArray[2],$dateArray[0]);
    
                $sec = $todayInSeconds-$inputInSeconds;
                $min = $sec / 60;
                $hour = $min / 60;
                $day = $hour / 24;
                $month = $day / $monthInDays;
                $year = $month / 12;
    
                $timeArray = [$year,$month,$day,$hour,$min,$sec];
                $timeNames = ['Years: ','Months: ','Days: ','Hours: ','Minutes: ','Seconds: '];
                
                $this->echoResult($timeArray, $timeNames);
                
            }
            else{
                $this->error();
            }
        }

        private function error(){
            echo '<div class=error>
                    <p class="error">Empty input field(s)!</p>
                </div>';
        }

        private function echoResult($timeArray, $timeNames){
            echo '<section>
                <h2 class="resultAge"> Your age: </h2>
                <ul class="time">';
                for ($i=0; $i < count($timeArray); $i++) { 
                    echo '<li>'.$timeNames[$i], $timeArray[$i].'</li>';
                }
                echo '</ul>
                </section>';
        }
    };

    if(isset($_POST["year"])){
        $age = new Age;

        $age->setYear($_POST['year']);
        $age->setMonth($_POST['month']);
        $age->setDay($_POST['day']);

        $age->result();
    }
?>


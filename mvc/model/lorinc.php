<h1>Enter your month salary to see how much money you earn compared to Lőrinc Mászáros.</h1>

<form name="lorinc" method="POST" action="" >
    <label for="inputSalary">Enter your month salary:</label>
    <input type="number" name="salary" min="50000" max="999999999" placeholder="Hungarian forint (Ft)">

    <input name="sub" value="Start" type="submit">
</form>


<?php
    class Lorinc{
        public $userIncome = 0;
        private $lorincIncome = 5066.67;

        public function getLorincIncome(){
            return $this->lorincIncome;
        }
    };

    if(isset($_POST["sub"])) {
        $l = new Lorinc; 

        $l->userIncome = $_POST["salary"];

        $error = "No value for input field";
        if($l->userIncome == null){
            echo '<p name="inputMoney" id="userInput" class="income">'.$error.'</p>';
        }
        else{
            echo '<p class="incomeStr">Your income:</p>';
            echo '<p name="inputMoney" id="userInput" class="income">'.$l->userIncome.'</p>';
        }
        echo '<p class="incomeStr">Lőrinc Mészáros income:</p>';
        echo '<p name="lorincMoney" id="lorinc" class="income">'.$l->getLorincIncome().'</p>';

        echo '<p class="incomeStr">Time passed:</p>';
        echo '<p name="secondCounter" id="seconds" class="income">0min 1sec</p>';
    }
?>

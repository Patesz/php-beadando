<?php
    class Nameday{
        private $file = null;
        private $name = null;

        public $month = null;
        public $day = null;

        function __construct()
        {
           $this->file = fopen("data/nevnapok.txt", "r");
        }

        function __destruct()
        {
            fclose($this->file);
        }

        public function nullFile(){
            if($this->file == null)
                return true;
            
            return false;
        }

        public function setName($name){
            $this->name = $name;
        }

        public function printTableHead(){
            echo '<table>
                    <thead>
                        <tr>
                            <th>Month</th>
                            <th>Day</th>
                            <th>Nameday(s)</th>
                        </tr>
                    </thead>
                    <tbody>';
        }

        public function printTableEnd(){
            echo '</tbody>
                </table>';
        }

        public function check(){
            while(!feof($this->file)){
                $line = fgets($this->file);
                $elements = explode("\t",$line);
                if($elements[0] == $this->month && $elements[1] == $this->day){
                    return true;
                }
            }
            return false;
        }

        public function nameday($month, $day){
            rewind($this->file);
            while(!feof($this->file)) {
                $line = fgets($this->file);
                $elements = explode("\t",$line);
                if($elements[0] == $month && $elements[1] == $day){
                    echo '<tr>
                            <td>'.$elements[0].'</td>
                            <td>'.$elements[1].'</td>
                            <td>'.$elements[2].'</td>
                        </tr>';
                }
            }
        }

        public function monthNameday(){
            while(!feof($this->file)) {
                $line = fgets($this->file);
                $elements = explode("\t",$line);
                if($elements[0] == $this->month){
                    echo '<tr>
                            <td>'.$elements[0].'</td>
                            <td>'.$elements[1].'</td>
                            <td>'.$elements[2].'</td>
                        </tr>';
                }
            }
        }

        public function printAllNameday(){
            $line=fgets($this->file);
            while(!feof($this->file)) {
                $line = fgets($this->file);
                $elements = explode("\t",$line);
                echo '<tr>
                        <td>'.$elements[0].'</td>
                        <td>'.$elements[1].'</td>
                        <td>'.$elements[2].'</td>
                    </tr>';
            }
        }

        public function searchName(){
            if($this->name == null){
                echo '<p class="error">No name has been given to search for!</p>';
                return;
            }
            $found = false;
            $line=fgets($this->file);
            while(!feof($this->file)) {
                $line = fgets($this->file);
                $elements = explode("\t",$line);
                $names = explode(",",$elements[2]);
                for ($i=0; $i < count($names); $i++) {
                    $trimmedName = trim($names[$i]);
                    if($this->name == $trimmedName || strcasecmp($this->name, $trimmedName) == 0){
                        if(!$found)
                            $this->printTableHead();
                        echo '<tr>
                            <td>'.$elements[0].'</td>
                            <td>'.$elements[1].'</td>
                            <td>'.$elements[2].'</td>
                        </tr>';
                        $found = true;
                    }
                }
            }
            if(!$found)
                echo '<p class="error">This name is not exist.</p>';
            else
                $this->printTableEnd();
        }
    };
?>


﻿<header>
    <div id="language">
    <form name="setLanguage" method="POST" action="">
        <input type="image" name="hun" src="img/hun.png" alt="HUN" width="30" height="30">
        <input type="image" name="eng" src="img/eng.png" alt="ENG" width="30" height="30">
    </form>
    </div>


<main>
    <?php
    function language($lng){
        if($lng=='introduction_hun'){
            welcome("nickname", "Üdv látogató");
            echo '<h2>Ezen az oldalon az alábbiakban felsorolt, vicces és esetleg hasznos oldalakat találod.</h2>
            </header>';

        }
        else{
            welcome("nickname", "Hello my friend");
            echo '<h2>In this site you can find find interesting and maybe useful pages.</h2>
            </header>';
        }

        $entireFile = file_get_contents("config/".$lng.".txt");

        $segments = explode("*",$entireFile);
        
        for($i = 0; $i < count($segments)-1; $i++){
            ${"part_".$i} = explode(";",$segments[$i]);
            echo '<h3>'.${"part_".$i}[0].'</h3>
                <p class="home">'.${"part_".$i}[1].'</p>';
        } 
    }

    function welcome($cookie_value, $welcomeString){
        if(isset($_COOKIE[$cookie_value])){
            echo '<h1>'.substr($welcomeString, 0, strpos($welcomeString,' ')).' '.$_COOKIE[''.$cookie_value.''].'!</h1>';
        }  
        else {
            echo '<h1>'.$welcomeString.'!</h1>';
        }
    }

    ?>
</main>


<?php
    if(isset($_POST['eng_x'])){
        language("introduction_eng");
    }
    elseif(isset($_POST['hun_x'])){
        language('introduction_hun');
    }
    else{
        language('introduction_eng');
    }

?>
    
 
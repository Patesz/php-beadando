<?php

class Cookie{
    private $cookie_name = "nickname";
    private $cookie_bgColor = "bgColor";

    public function setNickname($cookie_value) {
        setcookie($this->cookie_name, $cookie_value, time() + (86400), "/"); // 86400 = 1 day
    }

    public function setBgColor($cookie_value) {
        setcookie($this->cookie_bgColor, $cookie_value, time() + (86400), "/");
    }
};

$cookie = new Cookie();

if (isset($_POST['submitName']) ){
    if (isset($_POST['inputName']) && trim($_POST['inputName'] != '' )){
        $userName = htmlentities($_POST['inputName']);
        $cookie->setNickname($userName);
    }
    else {
        echo '<p class=error>Name input field is empty! :( </p>';
    }
}

if (isset($_POST['changeColor']) ) {
    $selectedColor = htmlentities($_POST['colorPicker']);
    $cookie->setBgColor($selectedColor);
}

?>


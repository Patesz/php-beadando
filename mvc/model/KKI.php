<h1>In this page you can calculate your KKI index and (weighted) average.</h1>

<form id="KKI_input" name="kki" method="POST" action="">
    <label for="inputSubject" class="subj">1. Subject: </label>
    <div id="duplicater0">
        <input class="subject" type="text" name="subject" placeholder="Subject name (optional)">
        <label for="inputGrade">Grade: </label>
        <input class="grade" type="number" name="grade" min="1" max="5" placeholder="Enter subject's grade"> 
        <label for="inputCredit">Credit: </label>
        <input class="credit" type="number" name="credit" min="1" max="12" placeholder="Enter credit value.">

        <input class="addSubject" type="button" value="+" onclick="duplicate()" />
    </div>
</form>

<div id="result">
</div>

<form name="calcKKI" method="POST" action="">
    <input class="selectFile" type="file" onchange="readFileAndGetResult()">
    <p id="alignPosition">OR</p> 
    <input class="mainButton" value="Calculate" type="button" onclick="getResult()" />
</form>
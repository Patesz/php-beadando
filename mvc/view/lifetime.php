<?php
    if(isset($_COOKIE['nickname'])){  
        echo'<h1>It shows you '.$_COOKIE['nickname'].', how old you are in years, months, days, and seconds.</h1>';
    }
    else{
        echo'<h1>It shows you, how old you are in years, months, days, and seconds.</h1>';
    }
?>

<form name="birth" method="POST" action="">
    <label for="inputYear">Enter your birth year:</label>
    <input type="number", name="year" min="1910" max= <?php echo '"'.date("Y").'" placeholder="1910-'.date("Y").'"' ?> >

    <label for="inputMonth">Enter your birth month: </label>
    <input type="number", name="month" min="1" max="12" placeholder="1-12">

    <label for="inputDay">Enter your birth day: </label>
    <input type="number", name="day" min="1" max="31" placeholder="1-31">

    <input name="sub" value = "Confirm" type="submit">
</form>

<script>
    var i = 0;
    var original = document.getElementById('duplicater0');
    var resultDiv = document.getElementById('result');
    
    function readFileAndGetResult(){
        let file = document.getElementsByClassName('selectFile')[0];
        let fileType = "txt";
        if(file.value.indexOf(fileType) !== -1){
            readFile(file);
        }
        else {
            alert("Sorry, only txt format is accepted.");
        }
    }

    function readFile(file) {
        var filee = file.target.files[0];
        if (!filee) {
            return;
        }
        var reader = new FileReader();
        reader.onload = function(e) {
            var contents = e.target.result;
            // Display file content
            displayContents(contents);
            alert(contents);
        };
        reader.readAsText(filee);
    }

    function duplicate() {
        let clone = original.cloneNode(true);       // "deep" clone
        
        createNewLabel();
        createRemoveButton();

        clone.id = "duplicater" + ++i;

        // or clone.id = ""; if the divs don't need an ID
        original.parentNode.appendChild(clone);
    }

    function createNewLabel() {
        let newLabel = document.createElement('label');
        newLabel.innerHTML = (i+2)+". Subject: ";    
        original.parentNode.appendChild(newLabel);
    }
    
    function deleteRecord() {
        let clone = original.cloneNode(true);
        clone.parentNode.removeChild();
    }

    function createRemoveButton() {
        let rmButton = document.createElement('button');
        rmButton.className = "removeButton" + i;
        rmButton.innerHTML = "-";  
        rmButton.onclick = "deleteRecord()";
        original.parentNode.appendChild(rmButton);
    }

    function getResult(){
        let count = 0;
        let acceptedCredit = 0, all = 0, sumCredit = 0;
        
        let nullField = false;
        while(document.getElementById("duplicater" + count) !== null){
            let gradeElem = document.getElementById("duplicater" + count).getElementsByClassName("grade")[0];
            let creditElem = document.getElementById("duplicater" + count).getElementsByClassName("credit")[0];

            let grade = gradeElem.value;
            let credit = creditElem.value;

            let g = parseFloat(grade);
            let c = parseFloat(credit);

            if(grade === NaN || credit === NaN || grade == null || credit == null || grade=='' || credit=='' ){
                nullField = true;
                printError();
                break;
            }

            if(g > 1 && g !== null){
                acceptedCredit += c;
                all += credit * g;
            }
            sumCredit += c;

            count++;
        }
        if(!nullField)
            printResult(acceptedCredit, sumCredit, all);
    }

    function printResult(acceptedCredit, sumCredit, all){
        var allCreditP;
        var weightedAvgP;
        var KKIP;

        if(!checkIfResultExists()){
            allCreditP = document.createElement('p');
            weightedAvgP = document.createElement('p');
            KKIP = document.createElement('p');
            
            allCreditP.id = "resultCredit";
            weightedAvgP.id = "resultWeightedAvg";
            KKIP.id = "resultKKI";
        }

        else{
            allCreditP = document.getElementById("resultCredit");
            weightedAvgP = document.getElementById("resultWeightedAvg");
            KKIP = document.getElementById("resultKKI");
        }

        resultDiv.parentNode.appendChild(allCreditP);
        resultDiv.parentNode.appendChild(weightedAvgP);
        resultDiv.parentNode.appendChild(KKIP);

        allCreditP.innerHTML = "All credits: " + sumCredit;
        weightedAvgP.innerHTML = "Weighted average: " + (all / acceptedCredit);
        KKIP.innerHTML = "KKI: " + (all/30 * acceptedCredit / sumCredit);
    }

    function checkIfResultExists(){
        if(document.getElementById("resultCredit")){    // if true, then element is already created
            return true;
        }
    }

    var error = null;
    function printError(){
        if(error == null){
            error = document.createElement('P');
            error.innerHTML = "Empty / null input field(s).";
            error.className = "error";

            resultDiv.parentNode.append(error);
        }
        showError();
        setInterval(hideError, 2000);     // calls function showAndHide
    }

    function showError(){
        error.style.display = 'block';
    }
    function hideError(){
        error.style.display = 'none';
    }


</script>

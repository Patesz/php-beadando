
<!---- Javascript function ---->
<script>                                                            // 1 month = 2 629 743.83 seconds

    const monthDivideSec = 1/2629744;                               // global variable
    var originalValue = 0;
    var income = 0;
    const lorincIncome = 5066.6666;

    function getOriginalInputValue(){
        var inputField = document.getElementById("userInput");
        originalValue = inputField.innerHTML;                                   // converts inputField content to integer and passes it to imputNum variable
        
        if(originalValue != null){
            income = parseFloat(monthDivideSec * originalValue);
            inputField.innerHTML = Number((income).toFixed(6));
        }
    }

    function calculateUserMoney(){
        let inputField = document.getElementById("userInput");                      // get input by it's tag name
        let money = parseFloat(inputField.innerHTML);                           // converts inputField content to integer and passes it to imputNum variable

        money += income;
        inputField.innerHTML = Number((money).toFixed(6));
    }

    function calculateLorincMoney() {
        let lorincField = document.getElementById("lorinc");
        let lorincMoney = parseFloat(lorincField.innerHTML);

        lorincMoney += lorincIncome;
        lorincField.innerHTML = Number((lorincMoney).toFixed(1));
    }

    let min = 0;
    let sec = 1;
    function displaySeconds() {
        let secondField = document.getElementById("seconds");
        let timeStr = secondField.innerHTML;
        let secStr = timeStr.substr(timeStr.indexOf(' ')+1);
        
        sec = parseInt(secStr.slice(0,-3));

        sec++;

        if(sec % 60 === 0 && sec != 0){
            let minStr = timeStr.substr(0,timeStr.indexOf(' '));
            min = parseInt(minStr.slice(0,-3));
            
            min++;
            sec = 0;
            secondField.innerHTML = min+"min "+sec+"sec"; 
        }
        else{
            secondField.innerHTML = min+"min "+sec+"sec";
        }
    }


    getOriginalInputValue();
    if(document.getElementById("userInput").innerHTML !== null){
        setInterval(calculateUserMoney, 1000);
    }
    setInterval(calculateLorincMoney, 1000);
    setInterval(displaySeconds, 1000);

</script>



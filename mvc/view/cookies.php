
<form name="nickName" method="POST" action="">
    <label for="name">Enter your name:</label>
    <input type="text" name="inputName">
    <input type="submit" name="submitName" value="Set nickname!">    
</form>

<hr><br>

<script src="js/jscolor.js"></script>

<form name="color" method="POST" action="" >
    Pick a color: <input class="jscolor" name="colorPicker" value="F9F9F9">
    <input class="btn" type="submit" name="changeColor" value="Change bg color">
</form>

<p>Check out <a href="http://jscolor.com/examples/">more color picker examples at jscolor.com</a>!</p>

<p class="warning">Warning: This page may use cookies on your system.</p>
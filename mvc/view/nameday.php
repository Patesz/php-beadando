<h1>You can search for namedays by date or by name.</h1>

<div id="nameday">
    <?php
        $nd = new Nameday;
        
        if (!$nd->nullFile()){
            if(isset($_POST["searchName"])){
                $nd->setName($_POST["name"]);
                $nd->searchName();
            }
            elseif(!isset($_POST["search"])){
                $month = date("m");
                $day = date("d");

                echo '<p class="todayNameday">Today\'s nameday(s):</p>';
                $nd->printTableHead();
                $nd->nameday($month, $day);
                $nd->printTableEnd();
            }
            else{
                $nd->month = $_POST["month"];
                $nd->day = $_POST["day"];

                if($nd->month != null && $nd->day != null){
                    if($nd->check()){
                        $nd->printTableHead();
                        $nd->nameday($nd->month, $nd->day);
                        $nd->printTableEnd();
                    }
                    else{
                        echo '<p class="error">Nothing found.</p>';
                    }
                }
                elseif($nd->month == null && $nd->day == null){
                    $nd->printTableHead();
                    $nd->printAllNameday();
                    $nd->printTableEnd();
                }
                elseif($nd->day == null){
                    $nd->printTableHead();
                    $nd->monthNameday();
                    $nd->printTableEnd();
                }
                else{   
                    echo '<p class="error">False input!</p>';
                }
            }
        }
        else{
            echo '<p class="error">Cannot open file!</p>';
        }
    ?>
</div>

<div id="namedayInput">
    <form name="birth" method="POST" action="">
        <label for="inputMonth">Enter month:</label>
        <input type="number" name="month" min="1" max="12" placeholder="1-12">
        <label for="inputMonth" place>Enter day: </label>
        <input type="number" name="day" min="1" max="31" placeholder="1-31">
        <input name="search" value="Search date" type="submit">

        <label for="inputName">Search for a name: </label>
        <input type="text" name="name" minlength="3" maxlength="18" placeholder="Name goes here.">
        <div id="searchName">
            <input name="searchName" value = "Search name" type="submit">
        </div>
    </form>
</div>
<?php

class Controller {

    private $page = "home";
    
    function __construct($page)
    {
        $this->page = $page;
    }

    public function model(){
        require_once 'mvc/model/'.$this->page.'.php';
    }

    public function view(){
        require_once 'mvc/view/'.$this->page.'.php';
    }

};
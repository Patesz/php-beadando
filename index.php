<?php

    include_once 'mvc/model/begin.php';

    if(isset($_GET['page']) && is_file('mvc/model/'.$_GET['page'].'.php'))
    {
        $page = $_GET['page'];
    }
    else
    {
        $page = "home";
    }

    require_once('mvc/controller/Controller.php');
    $c = new Controller($page);
    $c->model();
    
    if(is_file('mvc/view/'.$page.'.php')){
        $c->view();
    }

    include_once 'mvc/model/end.php';

?>

